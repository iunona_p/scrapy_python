import re
from urllib.parse import urljoin

import scrapy

from first_scrapy.items import FirstScrapyItem


class AmazonSpider(scrapy.Spider):
    name = "test_amazon"
    start_urls = [
        "https://www.amazon.de/gp/bestsellers/ce-de/ref=zg_bs_nav_0/261-1162755-1928137"
    ]

    def parse(self, response):
        for sel in response.xpath('//ol[@id="zg-ordered-list"]/li[@class="zg-item-immersion"]'):
            item = FirstScrapyItem()
            item['name'] = sel.css('img::attr(alt)').extract()
            item['rating'] = sel.css('span.a-icon-alt::text').extract()
            item['ASIN'] = sel.xpath('.//a[@class="a-link-normal"]/@href').re('(?<=product-reviews\/).*$')
            item['numOfReviews'] = sel.css('a.a-size-small.a-link-normal::text').extract()
            item['url'] = urljoin('https://www.amazon.de',
                                  ''.join(sel.css('a.a-link-normal.a-text-normal::attr(href)').extract()))
            item['price'] = sel.css('span.p13n-sc-price::text').extract()
            yield item
